import { createContext, useContext, useState, useCallback } from 'react';

export const FlashContext = createContext();
let flashTimer; //Global variable: this component is an internal implementation value that has no connections to anything that is visible on the page that might need to re-render, so that's the reason

export default function FlashProvider({ children }) {
  const [flashMessage, setFlashMessage] = useState({});
  const [visible, setVisible] = useState(false);

  // useCallback() hook function takes the function to memoize as the first argument, and a dependency list similar to the one used in the useEffect() callback as second argument. As long as the dependencies declared in the second argument don't change, the function will not change.
  //The hideFlash() was moved above flash(), because flash() calls it, which means that it needs to be listed as a dependency.
  const hideFlash = useCallback(() => {
    setVisible(false);
  }, []);

  const flash = useCallback((message, type, duration = 10) => {
    if (flashTimer) {
      clearTimeout(flashTimer); //starts by checking if there is an active flash timer. If there is a timer, that means that there is currently an alert on display that is going to be replaced. In that case the timer needs to be canceled, since a new timer will be created for the new alert.
      flashTimer = undefined;
    }
    setFlashMessage({message, type});
    setVisible(true);
    if (duration) { // the caller can pass 0 to skip the timer creation and display an alert that remains visible until the user closes it manually.
      flashTimer = setTimeout(hideFlash, duration * 1000);
    }
  }, [hideFlash]);

  // In React, when a prop needs to be assigned a value that is an object, two sets of braces are required. The outer pair of braces is what tells the 
  //JSX parser that the value of the prop is given as a JavaScript expression. The inner pair of braces are the object braces. The elements of the object, 
  //which are normally provided in key: value format, are in this case given as simple variables, using the object property shorthand, which takes the key and the value from the same variable.

  //flash is the function that components can use to flash a message to the page.
  return (
    <FlashContext.Provider value={{flash, hideFlash, flashMessage, visible}}>
      {children}
    </FlashContext.Provider>
  );
}

export function useFlash() {
    return useContext(FlashContext).flash;
  }