import { createContext, useContext, useCallback, useMemo } from 'react';
import MicroblogApiClient from '../MicroblogApiClient';
import { useFlash } from './FlashProvider';

const ApiContext = createContext(); //A React context is created

//The component renders the context's ApiContext.Provider, and puts its own children inside it. 
//This effectively enables all the child components to access the context.
export default function ApiProvider({ children }) {
  const flash = useFlash();

  const onError = useCallback(() => {
    flash('An unexpected error has occurred. Please try again.', 'danger');
  }, [flash]);

  //The api object cannot be memoized with useCallback(), which only works with functions. 
  //The useMemo() hook is a more generic version of useCallback() that can be used to memoize values of any type.
  const api = useMemo(() => new MicroblogApiClient(onError), [onError]);

  return (
    <ApiContext.Provider value={api}> 
      {children}
    </ApiContext.Provider>
  );
}

//Custom hook function that encapsulates the useContext() call
export function useApi() {
  return useContext(ApiContext); //To access the value of a context (here api), use the useContext hook
}