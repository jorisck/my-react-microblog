import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import { useState, useEffect } from 'react';
import FlashProvider from './FlashProvider';
import ApiProvider from './ApiProvider';
import UserProvider from './UserProvider';
import { useUser } from './UserProvider';


// Create a mock for fetch()
// The mocking in this case is done in the beforeEach() handler function, so that it applies to all the test in the file
// Since the purpose of mocking is to prevent network access from actually reaching a remote server, the fetch() function is the target to mock.
// Before this function is modified, it is a good idea to save the original in a global variable, so that it can be restored after the tests are done
const realFetch = global.fetch;

beforeEach(() => {

// jest.fn() function creates a general purpose fake function that can be used to replace any function in the application
// Functions created with jest.fn() have the interesting property that they record calls made to them, so these calls can then be checked in the test assertions
// Jest mocks can be programmed to return specific values, as needed by each test
  global.fetch = jest.fn(); 

});

// To keep things tidy, the afterEach() handler reverts the mock back to the original function.
// Since the MicroblogApiClient class stores access tokens in local storage (for handling refresh token), it is also a good practice to clear any data the might have been stored during a test when the test is done
afterEach(() => {
  global.fetch = realFetch;
  localStorage.clear();
});

test('logs user in', async () => {
    const urls = []; // The urls array is going to be used to collect all the URLs that the application calls fetch() on
  
    global.fetch
      .mockImplementationOnce(url => { //Accepts a function that will be used as an implementation of the mock for one call to the mocked function. Can be chained so that multiple function calls produce different results.
        urls.push(url);
        return {
          status: 200,
          ok: true,
          json: () => Promise.resolve({access_token: '123'}),
        };
      })
      .mockImplementationOnce(url => {
        urls.push(url);
        return {
          status: 200,
          ok: true,
          json: () => Promise.resolve({username: 'susan'}),
        };
      });
  
    // The Test component created by the test calls the login() function obtained 
    // from the useUser() hook in a side effect function, and renders the username of the logged-in user.
    const Test = () => {
      const { login, user } = useUser();
      useEffect(() => {
        (async () => await login('username', 'password'))();
      }, []);
      return user ? <p>{user.username}</p> : null;
    };
  
    // For the render portion of this test, the FlashProvider, ApiProvider and UserProvider 
    // components are added as wrappers to Test, so that all the hooks and contexts required during the test are available.
    render(
      <FlashProvider>
        <ApiProvider>
          <UserProvider>
            <Test />
          </UserProvider>
        </ApiProvider>
      </FlashProvider>
    );
  
    const element = await screen.findByText('susan'); // Note that the findBy...() set of functions are asynchronous, so they need to be awaited.
    expect(element).toBeInTheDocument();
    expect(global.fetch).toHaveBeenCalledTimes(2);
    expect(urls).toHaveLength(2);
    expect(urls[0]).toMatch(/^http.*\/api\/tokens$/);
    expect(urls[1]).toMatch(/^http.*\/api\/me$/);
  });

  test('logs user in with bad credentials', async () => {
    const urls = [];
  
    global.fetch
      .mockImplementationOnce(url => {
        urls.push(url);
        return {
          status: 401,
          ok: false,
          json: () => Promise.resolve({}),
        };
      });
  
    const Test = () => {
      const [result, setResult] = useState();
      const { login, user } = useUser();
      useEffect(() => {
        (async () => {
          setResult(await login('username', 'password')); // the login() function - returned by the useUser() hook - returns the string 'ok' when the login was successful, 'fail' when the login was invalid, and 'error' when an unexpected error occurred during the request.
        })();
      }, []);
      return <>{result}</>;
    };
  
    render(
      <FlashProvider>
        <ApiProvider>
          <UserProvider>
            <Test />
          </UserProvider>
        </ApiProvider>
      </FlashProvider>
    );
  
    const element = await screen.findByText('fail');
    expect(element).toBeInTheDocument();
    expect(global.fetch).toHaveBeenCalledTimes(1);
    expect(urls).toHaveLength(1);
    expect(urls[0]).toMatch(/^http.*\/api\/tokens$/);
  });

  test('logs user out', async () => {
    // This test presents a new challenge, because to be able to test that a user can log out, the user must be first logged in. Instead of repeating a login procedure as in previous tests, this time the test installs a fake access token in local storage, which will make the application believe that it is being started on a browser on which the user is logged in already.
    localStorage.setItem('accessToken', '123');
  
    // The mocked fetch() function for this test includes two calls. The first mocks the response to the /api/me request issued by the UserProvider component. A second mocked is response is included for the token revocation request issued during logout.
    global.fetch
      .mockImplementationOnce(url => {
        return {
          status: 200,
          ok: true,
          json: () => Promise.resolve({username: 'susan'}),
        };
      })
      .mockImplementationOnce((url) => {
        return {
          status: 204,
          ok: true,
          json: () => Promise.resolve({}),
        };
      });
  
    // The Test component used in this test is more complex than before. The component renders a page with the username of the logged-in user and a button to log out. When the button is clicked, the component re-renders with just the text "logged out".
    const Test = () => {
      const { user, logout } = useUser();
      if (user) {
        return (
          <>
            <p>{user.username}</p>
            <button onClick={logout}>logout</button>
          </>
        );
      }
      else if (user === null) {
        return <p>logged out</p>;
      }
      else {
        return null;
      }
    };
  
    render(
      <FlashProvider>
        <ApiProvider>
          <UserProvider>
            <Test />
          </UserProvider>
        </ApiProvider>
      </FlashProvider>
    );
  
    const element = await screen.findByText('susan');
    const button = await screen.findByRole('button');
    expect(element).toBeInTheDocument();
    expect(button).toBeInTheDocument();
  
    userEvent.click(button);
    const element2 = await screen.findByText('logged out'); // Note that this is issued with the asynchronous findBy...(), to wait for React to run all asynchronous operations and update the page.
    expect(element2).toBeInTheDocument();
    expect(localStorage.getItem('accessToken')).toBeNull();
  });