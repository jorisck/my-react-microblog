import { createContext, useContext, useState, useEffect, useCallback } from 'react';
import { useApi } from './ApiProvider';

const UserContext = createContext();

export default function UserProvider({ children }) {
    const [user, setUser] = useState();
    const api = useApi();
  
    useEffect(() => {
      (async () => {
        if (api.isAuthenticated()) {
          const response = await api.get('/me');
          setUser(response.ok ? response.body : null);
        }
        else {
          setUser(null);
        }
      })();
    }, [api]);
  
    const login = useCallback(async (username, password) => {
      const result = await api.login(username, password);
      if (result === 'ok') {
        const response = await api.get('/me');
        setUser(response.ok ? response.body : null);
        return response.ok;
      }
      return result;
    }, [api]);
  
    const logout = useCallback(async () => {
      await api.logout();
      setUser(null);
    }, [api]);
  
    return (
      <UserContext.Provider value={{ user, setUser, login, logout }}>
        {children}
      </UserContext.Provider>
    );
  }

  // For this hook, the entire object shared in the context is returned. The components accessing the context can use a destructuring assignment to obtain the attributes that they need.
export function useUser() {
    return useContext(UserContext);
  }
