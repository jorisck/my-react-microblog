const BASE_API_URL = process.env.REACT_APP_BASE_API_URL;

export default class MicroblogApiClient {
  constructor(onError) {
    this.onError = onError; //he caller passes an error handler,
    this.base_url =  BASE_API_URL + '/api';
  }

  async request(options) {
    let response = await this.requestInternal(options);
    if (response.status === 401 && options.url !== '/tokens') {//Checking the URL is useful to avoid a possible endless loop 
                                                                //if the refresh token endpoint also fails with a 401 status code, maybe due to an expired or missing refresh token.
      const refreshResponse = await this.put('/tokens', {
        access_token: localStorage.getItem('accessToken'), // To handling Refresh Tokens
      });
      if (refreshResponse.ok) {
        localStorage.setItem('accessToken', refreshResponse.body.access_token); // To handling Refresh Tokens
        response = await this.requestInternal(options);
      }
    }
    if (response.status >= 500 && this.onError) {
      this.onError(response);//The error handler is called when the response to a request has a 500 or higher status code, which according to the HTTP specification are associated with server failures.
    }
    return response;
  }

  async requestInternal(options) {
    let query = new URLSearchParams(options.query || {}).toString();
    if (query !== '') {
      query = '?' + query;
    }

    let response;
    try {
      response = await fetch(this.base_url + options.url + query, {
        method: options.method,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),
          ...options.headers,
        },
        credentials: options.url === '/tokens' ? 'include' : 'omit', //The token refresh requests that are sent to Microblog API must include the refresh token cookie that the API set when the user first authenticated. 
                                                                    // The fetch() function used to make the requests does not send cookies by default, so it needs to be told to do it for this request by adding the credentials option.
                                                                    // The credentials option is set to include only when the URL is that of the refresh token endpoint. In all other cases it is best to avoid sending unnecessary cookies by setting this option to omit.
        body: options.body ? JSON.stringify(options.body) : null,
      });
    }
    catch (error) {
      response = {
        ok: false,
        status: 500,
        json: async () => { return {
          code: 500,
          message: 'The server is unresponsive',
          description: error.toString(),
        }; }
      };
    }

    return {
      ok: response.ok,
      status: response.status,
      body: response.status !== 204 ? await response.json() : null
    };
  }

  async get(url, query, options) {
    return this.request({method: 'GET', url, query, ...options});
  }

  async post(url, body, options) {
    return this.request({method: 'POST', url, body, ...options});
  }

  async put(url, body, options) {
    return this.request({method: 'PUT', url, body, ...options});
  }

  async delete(url, options) {
    return this.request({method: 'DELETE', url, ...options});
  }

  //The Authorization header used in this method follows 
  //the Basic Authentication format defined by the HTTP specification, 
  //which requires encoding the username and password as a base64 string, 
  //done here by the btoa() function available in JavaScript.
  async login(username, password) {
    const response = await this.post('/tokens', null, {
      headers: {
        Authorization:  'Basic ' + btoa(username + ":" + password)
      }
    });
    if (!response.ok) {
      return response.status === 401 ? 'fail' : 'error';
    }
    localStorage.setItem('accessToken', response.body.access_token);
    return 'ok';
  }

  async logout() {
    await this.delete('/tokens');
    localStorage.removeItem('accessToken');
  }

  isAuthenticated() {
    return localStorage.getItem('accessToken') !== null;
  }
}
