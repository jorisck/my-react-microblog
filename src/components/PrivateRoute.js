import { useLocation, Navigate } from 'react-router-dom';
import { useUser } from '../contexts/UserProvider';

export default function PrivateRoute({ children }) {
    const { user } = useUser();
    const location = useLocation(); //a hook React-Router provides from to determine what is the current URL
  
    if (user === undefined) {
      return null;
    }
    else if (user) {
      return children;
    }
    else {
      const url = location.pathname + location.search + location.hash;
      return <Navigate to="/login" state={{next: url}} /> //The Navigate component supports a state prop in which the application can store any custom data to be preserved in the location object
    }
  }