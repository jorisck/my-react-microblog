import { useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { useApi } from '../contexts/ApiProvider'; //Since this function was declared as a non-default export in ApiProvider.js, it has to be imported using a destructuring assignment.
import Post from './Post';
import More from './More';
import Write from './Write';

export default function Posts({content='feed', write}) {
    const [posts, setPosts] = useState();
    const [pagination, setPagination] = useState();
    const api = useApi();

    let url;
    switch (content) {
      case 'feed':
      case undefined:
        url = '/feed';
        break;
      case 'explore':
        url = '/posts';
        break
      default:
        url = `/users/${content}/posts`;
        break;
    }
    
    useEffect(() => {
        (async () => {
          const response =await api.get(url);
          if (response.ok) {
            setPosts(response.body.data);
            setPagination(response.body.pagination);
          }
          else {
            setPosts(null);
          }
        })();
      }, [api, url]); //Among the second argument there is [api]. As you already learned, this array is used to tell 
                // React what are the dependencies of the side effect function. 
                //With an empty array, the function only ran during the first render of the component. 
                //When dependencies are given, a change in a dependency will make React call 
                //the side effect function again, so that the component is always up-to-date.
    
    const loadNextPage = async () => {
      const response = await api.get(url, {
        after: posts[posts.length - 1].timestamp
      });
      if (response.ok) {
        setPosts([...posts, ...response.body.data]);
        setPagination(response.body.pagination);
      }
    };

    const showPost = (newPost) => {
      setPosts([newPost, ...posts]); // the posts state variable is updated to contain the new post as the first post, with all the other posts after it. The spread operator is used to generate the updated post list.
    };

    return (
      <>
        {write && <Write showPost={showPost} />}
        {posts === undefined ?
          <Spinner animation="border" />
        :
            <>
                {posts === null ?
                    <p>Could not retrieve blog posts.</p>
                :
                    posts.map(post => <Post key={post.id} post={post} />)

                }
                <More pagination={pagination} loadNextPage={loadNextPage} />
            </>
        }
      </>
    );
  }