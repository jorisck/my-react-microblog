import { useState, useEffect } from 'react';

const secondsTable = [ //determining which of these units is the best to use in the relative time
  ['year', 60 * 60 * 24 * 365],
  ['month', 60 * 60 * 24 * 30],
  ['week', 60 * 60 * 24 * 7],
  ['day', 60 * 60 * 24],
  ['hour', 60 * 60],
  ['minute', 60],
];

const rtf = new Intl.RelativeTimeFormat(undefined, {numeric: 'auto'}); //generate the actual text of the relative time

//getTimeAgo is defined outside the TimeAgo component because it is a standalone function that does not need to be different for each instantiation of the component, 
//and it also does not need to change when the component re-renders.
function getTimeAgo(date) {//helper function accepts a Date object and finds the best relative units to use to render it
    const seconds = Math.round((date.getTime() - new Date().getTime()) / 1000);
    const absSeconds = Math.abs(seconds); //For dates that are in the past, the result of this calculation is going to be negative,
    let bestUnit, bestTime, bestInterval;
    for (let [unit, unitSeconds] of secondsTable) {
      if (absSeconds >= unitSeconds) {
        bestUnit = unit;
        bestTime = Math.round(seconds / unitSeconds);
        bestInterval = unitSeconds / 2;
        break;
      }
    };
    if (!bestUnit) {
      bestUnit = 'second';
      bestTime = parseInt(seconds / 10) * 10;
      bestInterval = 10; //but to avoid refreshing the time every second an interval of 10 seconds is used instead.
    }
    return [bestTime, bestUnit, bestInterval];
  }

export default function TimeAgo({ isoDate }) {
    const date = new Date(Date.parse(isoDate)); //creates a Date object, by parsing the isoDate prop, which is the string representation of the post's date in ISO 8601 format. 
    const [time, unit, interval] = getTimeAgo(date); //obtain the time, the units and the interval to use when rendering.
    // The usage of useState() in this component is different from the previous one, because only the setter function is stored. 
    //This usage solves a very specific need that this component has, which is to force itself to re-render even though none of 
    //the inputs ever change. React only re-renders components when their props or state variables change, 
    //so the only way to force a re-render is to create a dummy state variable that is not used anywhere, but is changed when a re-render is needed.
    const [, setUpdate] = useState(0); // to automatically update Date as time passes
  
    useEffect(() => { //side effect function
      const timerId = setInterval( //https://www.w3schools.com/jsref/met_win_setinterval.asp or https://developer.mozilla.org/en-US/docs/Web/API/setInterval
        () => setUpdate(update => update + 1),// here React will call the setter, at the interval define by setInterval, and pass the current value of the state to this function
        interval * 1000 //the set interval function second param is set in ns
      );
      return () => clearInterval(timerId); //Sometimes, side effect functions allocate resources, such as the interval timer here, and these resources need to be released when the component is removed from the page, to avoid resource leaks.
      // if we don't do that the timer will continue to run even if the component is not ever used.
    }, [interval]);//How inteval will be updated ? the set interval function above will rerender the compoanant a t a difined interval.
                //During this rerendering, the getTimeAgo will be call and if needed the interval value will be change ==> the side Effect funtion will be call again. 
    return (
      <span title={date.toString()}>{rtf.format(time, unit)}</span>
    );
  }